# Included schemata

- Bibliographic data in CSLJSON: [csl-data.json](https://github.com/citation-style-language/schema/blob/master/schemas/input/csl-data.json)
- CSL XML schema: [csl.rng](https://github.com/citation-style-language/schema/tree/master/schemas/styles), converted to rng by [jingtrang](https://pypi.org/project/jingtrang/) (ct. [StackOverflow](https://stackoverflow.com/questions/1254919/how-do-i-validate-xml-document-using-compact-relax-ng-schema-in-python))