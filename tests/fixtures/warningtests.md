# testdata 0

a[@citekey1] and b[@citekey4]
---
# testdata 1

a[^@citekey1] and b[^@citekey4]
---
# testdata 2

a[^1] b[^2]

[^1]: @citekey1
[^2]: @citekey4
---
# testdata 3

a[^1] b[^2]

[^1]: @citekey1
