# testdata 0

a[@citekey1] and b[@citekey2] and c[prefix @citekey3, suffix] and c[no_key] and (mail)[mailto:someone@somewhere.cc]
---
# testdata 1

a[^@citekey1] and b[^prefix @citekey2, suffix] and c[^no_key] and d[^@unknownkey]
---
# testdata 2

a[^1] and b[^1] and c[^3] and d[^4] and e[^5]

[^1]: @citekey1
[^2]: prefix @citekey2, suffix
[^3]: @unknownkey
[^4]: no_key
This [^6]: should not produce a match
---
# testdata 3

a[@citekey1;@citekey2;@citekey3] after
b[prefix;@citekey1:post1;post2] after
c[prefix @citekey1 suffix; pre2 @citekey2: post2] after
d[@citekey1: irregular suffix @citekey2] after
e[@citekey1 suffix @invalid] after
f[@invalid1; @invalid2]
---
# testdata 4

a[^@citekey1;@citekey2;@citekey3] after
b[^prefix;@citekey1:post1;post2] after
c[^prefix @citekey1 suffix; pre2 @citekey2: post2] after
d[^@citekey1: irregular suffix @citekey2] after
e[^@citekey1 suffix @invalid] after
f[^@invalid1; @invalid2]
---
# testdata 5

a[^1] after
b[^2] after
c[^3] after
d[^4] after
e[^5] after
f[^6]

[^1]: @citekey1;@citekey2;@citekey3
[^2]: prefix;@citekey1:post1;post2
[^3]: prefix @citekey1 suffix; pre2 @citekey2: post2
[^4]: @citekey1: irregular suffix @citekey2
[^5]: @citekey1 suffix @invalid
[^6]: @invalid1; @invalid2
---
# testdata 6

abc `a[@citkey2]` a[@citekey1]

```text
b[@citekey1]
```

\`c[@citekey2]

```
d[@citekey3]
```
---
# testdata 7

abc `a[^@citkey2]` a[^@citekey1]

```text
b[^@citekey1]
```

\`c[^@citekey2]

```
d[^@citekey3]
```
---
# testdata 8

abc `a[^1]` a[^1]

```text
b[^1]
[^1]: no match
```

abc `c[^2]` \`c[^2]

```
d[^2]
[^2]: no match
```

[^1]: @citekey1
[^2]: @citekey2
---
# testdata 9

```
[BIBLIOGRAPHY]
[FOOTNOTES]
```
  [BIBLIOGRAPHY]
  [FOOTNOTES]
  
[BIBLIOGRAPHY]
[FOOTNOTES]
