# testdata 0

a [@leonce] and b [prefix @sport, suffix] and and c[no_key] and (mail)[mailto:someone@somewhere.cc]

---

# testdata 1

a[^1] and b[^1] and c[^abc]

[^abc]: @leonce
[^1]: @sport

and c [^3] and d[^5]

[^3]: ct. @leonce, p. 42
[^4]: @daswerk

---

# testdata 2

a[^ct. @leonce, p. 12] and b[^no key]

[FOOTNOTES]

---

# testdata 3

a[^ct. @leonce, p. 12] and b[^no key]

[BIBLIOGRAPHY]
