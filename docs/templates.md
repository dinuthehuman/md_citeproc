# Templates

The rendering process of footnotes, footnote links and inline references uses Jinja templates to configure the exact markup of every replaced token. In the extensions constructor, the default templates can be overridden with custom templates. The same variable names must be used in a custom template. The class names in the templates can be used to style the rendered markup.

## Enumeration template

Used only if `output=OutputStyle.NUM_FOOTNOTES`

Specifies the markup for the enumerated link to a footnote in the text. The default template looks like this:

```html
<sup class="csl-footnote-anchor" id="csl-footnote-anchor-{{n}}">
    <a href="#csl-footnote-{{n}}">{{n}}</a>
</sup>
```

The variable `n` represents the current enumeration value as an `int`.

## Footnote template

Used only if `output=OutputStyle.NUM_FOOTNOTES`

Specifies the markup for each entry in the footnote list. The default template looks like this:

```html
<div id="csl-footnote-{{n}}" class="csl-footnote-entry">
    <a href="#csl-footnote-anchor-{{n}}" class="csl-footnote-number">
        {{n}}
    </a> 
    {{content}}
</div>
```

The variable `n` represents the current enumeration value as an `int`. The variable `content` represents the rendered footnote as `str`.

## Inline template

Used only if `output=OutputStyle.INLINE`

Specifies the markup of each inline-rendered reference. The default template looks like this:

```html
<span class="csl-inline">{{content}}</span>
```

The variable `content` represents the rendered reference as `str`.
