# Warnings

If citekeys are missing or if a footnote link points to a footnote that does not exist, a warning of the type `CiteprocWarning` is created and appended to a list. To get all warnings after the conversion process, call `get_warnings()` on the extension instance. Warnings can be converted to `str` for logging or output. Warnings can also be raised as Exceptions of the type `CiteprocStrictException` with `raise_()` called on a warning object. If `strict=True` is set in the extensions constructor, every warning is raised as an exception.
