# Notation Styles

There are a few competing standards on how to denote footnotes and bibliographic references in Markdown documents, with their specific advantages and disadvantages. By setting the `notation` parameter in `CiteprocExtension`, the extension is configurable to look for a specific notation, supporting three different styles.

## Citekeys

Generally, all notations are centered around the use of citekeys - unique identifiers referencing a single bibliographic entry in your library and saved to the `id` attribute in CSLJSON. These identifiers can be random, but exporting algorithms usually create a more or less humanly readable amalgamation of the authors name and the entries title whenever possible. To declare a citekey in the markdown document, it is started with the character `@`

!!!note
    If a text string in the Markdown document is identified as a citekey but that specific citekey is missing in the database, it is left untreated and a [warning](../warnings) is emitted. This behaivor is common to all notation styles.

## Prefixes & Suffixes

Each reference can contain a prefix before and a suffix after a citekey, i.e.

<pre><code class="language-text hljs plaintext">Some mention of gin.<span style="color: #E74C3C;">[ct. @fitzgeraldGreatGatsby2014, p. 15]</span></code></pre>

Prefixes and suffixes will be prepended or appended to the rendered reference.

## Inline Notation

Set by `notation=NotationStyle.INLINE`

The reference is set directly inline in square brackets, i.e.

<pre><code class="language-text hljs plaintext">haven't had the advantages that you've had.<span style="color: #E74C3C;">[@fitzgeraldGreatGatsby2014]</span></code></pre>

Since this notation is rather unspecific, a potential reference **containing no citekeys is ignored** in this notation to avoid false positives.

## Footnote Notation

Set by `notation=NotationStyle.FOOTNOTE`

References are denoted as explicit footnotes using an alphanumeric token:

<pre><code class="language-text hljs plaintext">haven't had the advantages that you've had.<span style="color: #E74C3C;">[^a]</span> The owls are not what they seem.<span style="color: #E74C3C;">[^b]</span>

<span style="color: #E74C3C;">[^a]: @fitzgeraldGreatGatsby2014</span>
<span style="color: #E74C3C;">[^b]: This was told to me by a giant in a dream.</span>
</code></pre>

The footnotes are enumerated in the order the explicit footnotes are used. Since this notation is a lot more specific, it can also be used to just write footnotes without a citation or a citekey at all.

## Inline-Footnote Notation

Set by `notation=NotationStyle.INLINE_FOOTNOTE`

Essentially, the same notation as `INLINE`, but needs a preceding `^`. This gives the notation the necessary specifity to allow using it to denote footnotes not containing a citekey, i.e.

<pre><code class="language-text hljs plaintext">haven't had the advantages that you've had.<span style="color: #E74C3C;">[^@fitzgeraldGreatGatsby2014]</span> The owls are not what they seem.<span style="color: #E74C3C;">[^This was told to me by a giant in a dream.]</span></code></pre>

