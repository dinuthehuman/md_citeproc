# Construction/Configuration

Since there are quite a few competing standards and conventions on how to denote citations and bibliographic references in Markdown and how to render them in the finished document, this extension offers a rather big set of configuration options.

The constructor of the extension has the following type-hinted signature:

```python
CiteprocExtension(
        csljson: Optional[Union[Path, str]] = None,
        cslfile: Optional[Union[Path, str]] = None,
        uncited: Optional[list[str]] = None,
        locale: str = "en-US",
        localedir: Union[CiteprocConfigDefaults, Path, str] = CiteprocConfigDefaults.BUILTIN,
        notation: Union[NotationStyle, str] = NotationStyle.INLINE,
        output: Union[OutputStyle, str] = OutputStyle.NUM_FOOTNOTES,
        num_template: Union[CiteprocConfigDefaults, jinja2.Template, Path, str] = CiteprocConfigDefaults.BUILTIN,
        footnote_template: Union[CiteprocConfigDefaults, jinja2.Template, Path, str] = CiteprocConfigDefaults.BUILTIN,
        inline_template: Union[CiteprocConfigDefaults, jinja2.Template, Path, str] = CiteprocConfigDefaults.BUILTIN,
        footnotes_token: str = "[FOOTNOTES]",
        bibliography_token: str = "[BIBLIOGRAPHY]",
        citeproc_executable: Union[CiteprocConfigDefaults, Path, str] = CiteprocConfigDefaults.AVAILABLE,
        strict: bool = False
    )
```

## Configuration Parameters

### `csljson`

Type: `Union[pathlib.Path, str]`<br />
Default: `None`

Path (as `str` or as `Path` object) of a JSON text file containing the library of bibliographic data in [CSLJSON](https://citeproc-js.readthedocs.io/en/latest/csl-json/markup.html). Bibliography Management software such as [Zotero](https://www.zotero.org/) is usually able to export interal libraries to this format. Zotero is even able to export data in this format on a `GET` request, provided the plugin [Better BibTeX for Zotero](https://retorque.re/zotero-better-bibtex/) is installed.

If this parameter is `None`, no rendering takes place (the extension can still be used to handle footnotes without citations).

### `cslfile`

Type: `Union[pathlib.Path, str]`
Default: `None`

Path (as `str` or as `Path` object) of a XML file containing the [CSL](https://citationstyles.org/) style to use to render citations.

If this parameter is `None`, no rendering takes place (the extension can still be used to handle footnotes without citations).

### `uncited`

Type: `Optional[list[str]]`<br />
Default: `None`

List of citekeys that are not used in the document but still added to the bibliography. The list entries must contain citekeys without a preceeding `@` character. One entry can contain multiple citekeys separated by `;`. A valid example would look like this:

```python
uncited = ['citekey1', 'citekey2;citekey3']
```

### `locale`

Type: `str`<br />
Default: `en-US`

[RFC5646](https://datatracker.ietf.org/doc/html/rfc5646)-compliant String to denote the locale to use for the rendering process.

### `localedir`

Type: `Union[CiteprocConfigDefaults, Path, str]`<br />
Default: `CiteprocConfigDefaults.BUILTIN`

Path (as `str` or as `Path` object) of a directory containing CSL locale files. This would ideally be a clone of the [CSL Locales Repository](https://github.com/citation-style-language/locales). If the default value `CiteprocConfigDefaults.BUILTIN` is used, a set of locale files bundled with the extension are unpacked and used during the rendering process. While this is probably fine in most situations, it has an impact on performance. If efficiency is an issue, you should use your own clone to avoid unpacking on every conversion.

### `notation`

Type: `Union[NotationStyle, str]`<br />
Default: `NotationStyle.INLINE`

Specify the notation style for bibliographic references and footnotes in your document, for details check [Notation Styles](../notationstyles). Use either an attribute of the enumation `NotationStyle` or a string that corelates to one of the enum attributes, namely one of the following values:

| Enum value                      | Correlating string |
|---------------------------------|--------------------|
| `NotationStyle.INLINE`          | `inline`           |
| `NotationStyle.FOOTNOTE`        | `footnote`         |
| `NotationStyle.INLINE_FOOTNOTE` | `inline_footnote`  |

### `output`

Type: `Union[OutputStyle, str]`<br />
Default: `NotationStyle.NUM_FOOTNOTES`

Specify the output style to use in the rendered document, for details, check [Output Styles](../outputstyles). Use either an attribute of the enumeration `OutputStyle` or a string that corelates to one of the attributes, namely one of the following values:

| Enum value                  | Correlating string |
|-----------------------------|--------------------|
| `OutputStyle.INLINE`        | `inline`           |
| `OutputStyle.NUM_FOOTNOTES` | `num_footnotes`    |

If `notation=NotationStyle.FOOTNOTE`, this parameter is ignored and the rendering method `OutputStyle.num_footnotes` is used.

### `num_template`

Type: `Union[CiteprocConfigDefaults, jinja2.Template, Path, str]`<br />
Default: `CiteprocConfigDefaults.BUILTIN`

Jinja template to use for footnote enumeration in the text of the rendered document, check [Templates](../templates) for details. The constructor tries to create a Jinja template from the parameter value in every way possible in the following order:

- If the default value is used, the template bundled with the extension is used.
- If a `Path` object or a string containing the path of an existing file is used, the file the path points to is read and used as a template.
- If a string is used (that is not the path of an existing file) the string is read directly as a template.

This parameter is irrelevant if `output=OutputStyle.INLINE`.

### `footnote_template`

Type: `Union[CiteprocConfigDefaults, jinja2.Template, Path, str]`<br />
Default: `CiteprocConfigDefaults.BUILTIN`

Jinja template used to render the footnotes in the rendered document, check [Templates](../templates) for details. The constructor tries to create a Jinja template from the parameter value in every way possible in the following order:

- If the default value is used, the template bundled with the extension is used.
- If a `Path` object or a string containing the path of an existing file is used, the file the path points to is read and used as a template.
- If a string is used (that is not the path of an existing file) the string is read directly as a template.

This parameter is irrelevant if `output=OutputStyle.INLINE`.

### `inline_template`

Type: `Union[CiteprocConfigDefaults, jinja2.Template, Path, str]`<br />
Default: `CiteprocConfigDefaults.BUILTIN`

Jinja template used to render inline citations in the rendered document, check [Templates](../templates) for details. The constructor tries to create a Jinja template from the parameter value in every way possible in the following order:

- If the default value is used, the template bundled with the extension is used.
- If a `Path` object or a string containing the path of an existing file is used, the file the path points to is read and used as a template.
- If a string is used (that is not the path of an existing file) the string is read directly as a template.

This parameter is irrelevant if `output=OutputStyle.NUM_FOOTNOTES`.

### `footnotes_token`

Type: `str`<br />
Default: `[FOOTNOTES]`

Token string to replace with the collection of rendered footnotes in the rendered document.

This parameter is irrelevant if `output=OutputStyle.INLINE`.

### `bibliography_token`

Type: `str`<br />
Default: `[BIBLIOGRAPHY]`

Token string to replace with the rendered bibliography in the rendered document.

### `citeproc_executable`

Type: `Union[CiteprocConfigDefaults, Path, str]`<br />
Default: `CiteprocConfigDefaults.AVAILABLE`

Specify an executable to use to render citations, as a `Path` object or as a string. Check `citeproc-cli` for details. If the default `CiteprocConfigDefaults.AVAILABLE` is used, first the `$PATH` is searched for a matching executable. If unsuccessful, the executable shiped with the exentsion is used (and an exception is thrown, if no bundled binary matches the system/architecture). If `CiteprocConfigDefaults.BUILTIN` is used, searching `$PATH` is omitted and the bundled binary is used directly.

The bundled binary must be unpacked before use. While this is probably fine in most situations, it has a performance impact. When efficency matters, an executable should be explicitly specified.

### `strict`

Type: `bool`<br />
Default: `False`

Instead of collecting a warning, throw a `CiteprocStrictException`. Check [Warnings](../warnings) for details.
