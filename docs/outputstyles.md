# Output Styles

Depending on the CSL file, a different form rendering might be required. This extension can handle inline rendering or footnote rendering of citations.

For the examples on this page, a very small [CSLJSON file](../example_data/lib.json) is used. As a style to render inline, the CSL file [Cambridge University Press (author-date)](../example_data/cambridge-university-press-author-date.csl) is used and for footnote-based rendering, the CSL file [Cambridge University Press (note)](../example_data/cambridge-university-press-note.csl) is used.

## Inline

Set by `output=OutputStyle.INLINE`

Citations are rendered inline directly in the text. The footnote token remains unmodified since there are no footnotes to render. Using this option, the following Markdown document...

<pre><code class="language-text hljs plaintext">any fresh incidents to record.<span style="color: #E74C3C;">[@shelleyFrankenstein2020, Letter 4]</span> Something completely different.<span style="color: #E74C3C;">[@missingCitekey]</span></code></pre>

...will be converted to the following HTML:

```html
<p>
    any fresh incidents to record.
    <span class="csl-inline">(Shelley 2020, Letter 4)</span> 
    Something completely different.
    <span class="csl-inline">@missingCitekey</span>
</p>
```

This setting is ignored for `notation=NotationStyle.FOOTNOTES`.

## Enumerated footnotes

Set by `output=OutputStyle.NUM_FOOTNOTES`

In the text, references are replaced with enumerated links, pointing to a footnote in a footnote list that replaces the footnote token.Using this option, the following Markdown document...

<pre><code class="language-text hljs plaintext">any fresh incidents to record.<span style="color: #E74C3C;">[@shelleyFrankenstein2020, Letter 4]</span> Something completely different.<span style="color: #E74C3C;">[@missingCitekey]</span>

<span style="color: #E74C3C;">[FOOTNOTES]</span></code></pre>

...will be converted to the following HTML:

```html
<p>
    I shall continue my journal concerning the stranger at intervals, should I have any fresh incidents to record.
    <sup class="csl-footnote-anchor" id="csl-footnote-anchor-1">
        <a href="#csl-footnote-1">1</a>
    </sup> 
    Something completely different.
    <sup class="csl-footnote-anchor" id="csl-footnote-anchor-2">
        <a href="#csl-footnote-2">2</a>
    </sup>
</p>
<p>
    <div id="csl-footnote-1" class="csl-footnote-entry">
        <a href="#csl-footnote-anchor-1" class="csl-footnote-number">1</a>
        M. Shelley, <i>Frankenstein.</i>, (G&#38;D Media, 2020), Letter 4.
    </div>
    <div id="csl-footnote-2" class="csl-footnote-entry">
        <a href="#csl-footnote-anchor-2" class="csl-footnote-number">2</a> 
        @missingCitekey
    </div>
</p>
```

This setting is always used for `notation=NotationStyle.FOOTNOTES`.
