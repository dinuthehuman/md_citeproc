# Todos

This is more a developers notepad than a part of the documentation but this still seems like an adequate place for this file.

- Schema validation on CSLJSON and CSL files
    - Schemata are already present on GitLab, not yet packaged with the extension
    - The process to validate CSL XML seems rather cumbersome
    - A bit of research in [sources.txt](../md_citeproc/assets/schemata/sources.md)
