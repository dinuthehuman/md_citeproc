import os
import shutil
from pathlib import Path
from invoke import task


own_dir = Path(__file__).parent.resolve()


@task
def distclean(c):
    os.chdir(own_dir.parent)
    for root, dirs, files in os.walk('./dist'):
        for f in files:
            os.unlink(os.path.join(root, f))
        for d in dirs:
            shutil.rmtree(os.path.join(root, d))


@task
def distbuild(c):
    os.chdir(own_dir.parent)
    c.run("python3 setup.py sdist bdist_wheel")


@task(pre=[distclean, distbuild])
def publish(c):
    os.chdir(own_dir.parent)
    c.run("twine upload dist/*")
